# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit rpm

DESCRIPTION="Axsem Microfoot Command Line debugger libraries."
HOMEPAGE="http://www.axsem.com"
LICENSE="axsem"
SLOT="0"
KEYWORDS="~amd64"
SRC_URI="https://bitbucket.org/bearsh/axsem/downloads/libmfdbg-${PV}-1.fc23.x86_64.rpm"

IUSE=""

S="${WORKDIR}"

RDEPEND="dev-cpp/glibmm
	dev-cpp/libxmlpp:2.6
	dev-embedded/libftdi:1
	dev-lang/tcl
	dev-libs/glib
	dev-libs/libsigc++
	dev-libs/libusb
	dev-libs/libxml2
"
DEPEND="${RDEPEND}"

QA_PRESTRIPPED="/usr/lib64/libaxsdb.so.0.0.0
	/usr/lib64/libmfdbg.so.0.0.0
	/usr/bin/axftdieeprog
"

src_install() {
	dobin usr/bin/axftdieeprog
	dolib.so usr/lib64/*
	dodoc usr/share/doc/${PN}/*
}

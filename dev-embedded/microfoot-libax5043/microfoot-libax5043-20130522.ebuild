# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="6"

SN="${PN/microfoot-/}"
DESCRIPTION="Axsem AX5043 Microfoot Microprocessor Library"
HOMEPAGE="http://www.axsem.com"
LICENSE="axsem"
SLOT="0"
KEYWORDS="~amd64"
SRC_URI="https://bitbucket.org/bearsh/axsem/downloads/${SN}src-${PV}.tar.gz"

IUSE=""

S="${WORKDIR}"

RDEPEND="dev-embedded/sdcc[mcs51,sdbinutils,device-lib]"
DEPEND="${RDEPEND}
	dev-embedded/microfoot-libmf
"

src_prepare() {
	default
	sed -i 's/sdcc-//g' "buildsdcc/Makefile" || die "sed failed"
	sed -i 's:../../libmf/source:/usr/share/microfoot/libmf/include:g' "buildsdcc/Makefile" || die "sed failed"
}

src_compile() {
	cd buildsdcc
	make
	make tar
}

src_install() {
	local datadir="${D}/usr/share"
	install -d ${datadir}/microfoot/${SN}/source
	tar -x -v -z -C ${datadir}/microfoot/${SN} -f "${DISTDIR}/${A}"
	install -d ${datadir}/microfoot/${SN}/include
	cp -r ${datadir}/microfoot/${SN}/source/*.h ${datadir}/microfoot/${SN}/include
	install -d ${datadir}/microfoot/${SN}/sdcc
	tar -x -v -z -C ${datadir}/microfoot/${SN}/sdcc -f buildsdcc/${SN}binsdcc.tar.gz
	rm -r ${datadir}/microfoot/${SN}/{buildiar,buildkeil,buildkeilx,doc}
	dodoc doc/*
}

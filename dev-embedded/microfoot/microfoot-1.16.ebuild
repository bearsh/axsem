# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit rpm

DESCRIPTION="Axsem Microfoot Command Line debugger."
HOMEPAGE="http://www.axsem.com"
LICENSE="proprietary"
SLOT="0"
KEYWORDS="~amd64"
SRC_URI="https://bitbucket.org/bearsh/axsem/downloads/${P}-1.fc23.x86_64.rpm"

IUSE=""

S="${WORKDIR}"

RDEPEND="dev-cpp/glibmm
	dev-cpp/libxmlpp:2.6
	dev-embedded/libftdi:1
	dev-embedded/libmfdbg
	dev-lang/tcl
	dev-libs/glib
	dev-libs/libsigc++
	dev-libs/libusb
	dev-libs/libxml2
"
DEPEND="${RDEPEND}"

QA_PRESTRIPPED="/usr/bin/ax51disass
	/usr/bin/axsdb
"

src_install() {
	dobin usr/bin/ax51disass
	dobin usr/bin/axsdb
	dodoc usr/share/doc/${PN}/*
	insinto /usr/share/
	doins -r usr/share/${PN}
}

# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="6"

DESCRIPTION="Axsem Microfoot Microprocessor Library"
HOMEPAGE="http://www.axsem.com"
LICENSE="axsem"
SLOT="0"
KEYWORDS="~amd64"
SRC_URI="https://bitbucket.org/bearsh/axsem/downloads/mflibsrc-${PV}.tar.gz"

IUSE=""

S="${WORKDIR}"

RDEPEND="dev-embedded/sdcc[mcs51,sdbinutils]"
DEPEND="${RDEPEND}
	dev-util/unifdef
"

src_prepare() {
	default
	sed -i 's/sdcc-//g' "buildsdcc/Makefile" || die "sed failed"
}

src_compile() {
	pushd buildsdcc >/dev/null
	make
	make tar
}

src_install() {
	local datadir="${D}/usr/share"
	install -d ${datadir}/microfoot/libmf/source
	tar -x -v -z -C ${datadir}/microfoot/libmf -f "${DISTDIR}/${A}"
	install -d ${datadir}/microfoot/libmf/builtsource
	cp buildsdcc/libmf/*.c ${datadir}/microfoot/libmf/builtsource
	install -d ${datadir}/microfoot/libmf/include
	cp -r ${datadir}/microfoot/libmf/source/*.h ${datadir}/microfoot/libmf/include
	rm -f ${datadir}/microfoot/libmf/include/radiodefs.h
	rm -f ${datadir}/microfoot/libmf/include/wrnum.h
	rm -f ${datadir}/microfoot/libmf/include/wtimer.h
	install -d ${datadir}/microfoot/libmf/sdcc
	tar -x -v -z -C ${datadir}/microfoot/libmf/sdcc -f buildsdcc/mflibbinsdcc.tar.gz
	rm -r ${datadir}/microfoot/libmf/{buildiar,buildkeil,buildkeilx}
	dodoc doc/*
}

# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="6"

DESCRIPTION="Axsem Microfoot Microprocessor SDCC Reentrant int, long and float library"
HOMEPAGE="http://www.axsem.com"
LICENSE="axsem"
SLOT="0"
KEYWORDS="~amd64"
SRC_URI="https://bitbucket.org/bearsh/axsem/downloads/libreentsrc-${PV}.tar.gz"

IUSE=""

S="${WORKDIR}"

RDEPEND="dev-embedded/sdcc[mcs51,sdbinutils,device-lib]"
DEPEND="${RDEPEND}"

src_prepare() {
	default
	sed -i 's/sdcc-//g' "buildsdcc/Makefile" || die "sed failed"
	sed -i "s:/usr/share/sdcc/lib/src:${S}/usr_share_sdcc_lib_src:g" "buildsdcc/Makefile" || die "sed failed"
	cp -R "/usr/share/sdcc/lib/src" "${S}/usr_share_sdcc_lib_src"
}

src_compile() {
	pushd buildsdcc >/dev/null
	make
	make tar
}

src_install() {
	local datadir="${D}/usr/share"
	install -d ${datadir}/microfoot/libreent/source
	tar -x -v -z -C ${datadir}/microfoot/libreent -f "${DISTDIR}/${A}"
	install -d ${datadir}/microfoot/libreent/sdcc
	tar -x -v -z -C ${datadir}/microfoot/libreent/sdcc -f buildsdcc/libreentbinsdcc.tar.gz
}
